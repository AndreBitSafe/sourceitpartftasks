package com.lessons.model;

public class Smartphone {

    private String brand;
    private String model;
    private int cost;

    public Smartphone(String brand, String model, int cost) {
        this.brand = brand;
        this.model = model;
        this.cost = cost;
    }

    public boolean isCheaper(Smartphone smartphone) {
        return this.cost < smartphone.cost;
    }

    public void printAllInfo() {
        System.out.println("--> Brand: " + this.brand);
        System.out.println("\tModel: " + this.model);
        System.out.println("\tCost: " + this.cost);
    }

    public boolean isThisThePhoneOfThisBrand(String inBrand) {
        return this.brand.equalsIgnoreCase(inBrand);
    }

    public boolean isThisThePhoneOfThisModel(String inModel) {
        return this.model.equalsIgnoreCase(inModel);
    }
}
