package com.lessons.model;

public class Store {
    private String name;
    private String address;
    private Smartphone[] sellingSmartphones;

    public Store(String name, String address, Smartphone[] sellingSmartphones) {
        this.name = name;
        this.address = address;
        this.sellingSmartphones = sellingSmartphones;
    }

    public Smartphone findCheapestPhone() {
        Smartphone chSmartphone = sellingSmartphones[0];
        for (Smartphone phone : sellingSmartphones) {
            if (phone.isCheaper(chSmartphone)) {
                chSmartphone = phone;
            }
        }
        return chSmartphone;
    }

    public void printInfoAboutStore() {
        System.out.println(">>> Store: " + this.name);
        System.out.println("\tAddress: " + this.address);
    }

    public void printPhonesOfThisBrand(String inBrand) {
        for (Smartphone smartphone : sellingSmartphones) {
            if (smartphone.isThisThePhoneOfThisBrand(inBrand)) {
                smartphone.printAllInfo();
            }
        }
    }

    public Smartphone findPhoneOfThisModel(String inModel) {
        for (Smartphone smartphone : sellingSmartphones) {
            if (smartphone.isThisThePhoneOfThisModel(inModel)) {
                return smartphone;
            }
        }
        return null;
    }
}
