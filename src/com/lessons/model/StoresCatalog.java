package com.lessons.model;

public class StoresCatalog {
    private Store[] stores;

    public StoresCatalog(Store[] stores) {
        this.stores = stores;
    }

    public void printCheapestSmartphone() {
        Store storeHasCheapestSmPhone = stores[0];
        for (Store store : stores) {
            if (store.findCheapestPhone().isCheaper(storeHasCheapestSmPhone.findCheapestPhone())) {
                storeHasCheapestSmPhone = store;
            }
        }
        storeHasCheapestSmPhone.printInfoAboutStore();
        storeHasCheapestSmPhone.findCheapestPhone().printAllInfo();
    }

    public void printAllPhonesOfThisBrand(String inBrand) {
        for (Store store : stores) {
            store.printInfoAboutStore();
            store.printPhonesOfThisBrand(inBrand);
        }
    }

    public void printAllPhonesOfThisModel(String inModel) {
        Store storeHasPhoneOfThisModel = stores[0];
        boolean wasFind = false;
        for (Store store : stores) {
            Smartphone phone = store.findPhoneOfThisModel(inModel);
            if (phone != null && phone.isCheaper(storeHasPhoneOfThisModel.findPhoneOfThisModel(inModel))) {
                storeHasPhoneOfThisModel = store;
                wasFind = true;
            }
        }
        if (wasFind) {
            storeHasPhoneOfThisModel.printInfoAboutStore();
            storeHasPhoneOfThisModel.findPhoneOfThisModel(inModel).printAllInfo();
        }else{
            System.out.println("Smartphone not found!!!");
        }
    }
}
