package com.lessons;

import com.lessons.data.StoresBuilder;
import com.lessons.model.StoresCatalog;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        StoresCatalog storesCatalog = new StoresCatalog(StoresBuilder.generateStores());

        System.out.println("Cheapest smartphone: ");
        storesCatalog.printCheapestSmartphone();

        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter brand to find phones: ");
        String inBrand = scanner.nextLine();
        storesCatalog.printAllPhonesOfThisBrand(inBrand);

        System.out.println("Enter model to find phones: ");
        String inModel = scanner.nextLine();
        storesCatalog.printAllPhonesOfThisModel(inModel);
    }
}
