package com.lessons.data;

import com.lessons.model.Smartphone;
import com.lessons.model.Store;

public final class StoresBuilder {
    private StoresBuilder() {
    }

    public static Store[] generateStores() {
        Store[] stores = new Store[5];
        {
            Smartphone[] smartphones = {
                    new Smartphone("Apple", "Iphone 10", 30900),
                    new Smartphone("Apple", "Iphone 11", 41900),
                    new Smartphone("Samsung", "Galaxy s10", 23750),
                    new Smartphone("Huawei", "P20", 33100),
                    new Smartphone("Huawei", "P10", 12100)
            };
            stores[0] = new Store("Paladium", "Petrova 90A", smartphones);
        }
        {
            Smartphone[] smartphones = {
                    new Smartphone("Apple", "Iphone 10", 31000),
                    new Smartphone("Apple", "Iphone 11", 42200),
                    new Smartphone("Samsung", "Galaxy s10", 22750),
                    new Smartphone("Huawei", "P20", 35100),
                    new Smartphone("Huawei", "P10", 18100)
            };
            stores[1] = new Store("Allo", "Nazarova 9B", smartphones);
        }
        {
            Smartphone[] smartphones = {
                    new Smartphone("Samsung", "Galaxy s10", 24770),
                    new Smartphone("Huawei", "P20", 33400),
                    new Smartphone("Huawei", "P10", 11100)
            };
            stores[2] = new Store("Cactus", "Soloveva 13", smartphones);
        }
        {
            Smartphone[] smartphones = {
                    new Smartphone("Apple", "Iphone 10", 31900),
                    new Smartphone("Apple", "Iphone 11", 40990),
                    new Smartphone("Huawei", "P20", 32100),
                    new Smartphone("Huawei", "P10", 11150)
            };
            stores[3] = new Store("Sitrus", "Sumska 112/3", smartphones);
        }
        {
            Smartphone[] smartphones = {
                    new Smartphone("Apple", "Iphone 10", 31900),
                    new Smartphone("Apple", "Iphone 11", 44900),
            };
            stores[4] = new Store("Smart World", "Grecova 120", smartphones);
        }
        return stores;
    }
}
